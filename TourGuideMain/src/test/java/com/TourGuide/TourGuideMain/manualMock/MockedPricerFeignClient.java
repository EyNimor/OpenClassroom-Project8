package com.TourGuide.TourGuideMain.manualMock;

import java.util.List;
import java.util.UUID;

import com.TourGuide.TourGuideMain.feignClient.PricerClient;
import com.TourGuide.TourGuideMain.model.Provider;

public class MockedPricerFeignClient implements PricerClient {

    List<Provider> providersList;

    public MockedPricerFeignClient(List<Provider> providersList) {
        this.providersList = providersList;
    }

    @Override
    public List<Provider> getPrice(String tripPricerApiKey, UUID userId, int numberOfAdults, int numberOfChildren,
            int tripDuration, int cumulatativeRewardPoints) {
        return providersList;
    }
    
}
