package com.TourGuide.TourGuideMain.manualMock;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.TourGuide.TourGuideMain.feignClient.GpsClient;
import com.TourGuide.TourGuideMain.model.Attraction;
import com.TourGuide.TourGuideMain.model.Location;
import com.TourGuide.TourGuideMain.model.VisitedLocation;

public class MockedGpsFeignClient implements GpsClient {

    List<Attraction> attractions;

    public MockedGpsFeignClient(List<Attraction> attractions) {
        this.attractions = attractions;
    }

    @Override
    public VisitedLocation getUserLocation(String userId) {
        return new VisitedLocation(UUID.fromString(userId), new Location(0.0D, 0.0D), new Date());
    }

    @Override
    public List<Attraction> getAllAttractions() {
        return attractions;
    }
    
}
