package com.TourGuide.TourGuideMain.manualMock;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.TourGuide.TourGuideMain.feignClient.RewardClient;

public class MockedRewardFeignClient implements RewardClient {

    Map<UUID, Integer> rewardsMap;

    public MockedRewardFeignClient(Map<UUID, Integer> rewardsMap) {
        this.rewardsMap = rewardsMap;
    }

    @Override
    public Map<UUID, Integer> getRewardPointsForMultipleAttractions(List<UUID> attractionsIds, String userId) {
        return rewardsMap;
    }

    @Override
    public Integer getAttractionRewardPoints(UUID attractionId, UUID userId) {
        return 199;
    }
    
}
