package com.TourGuide.TourGuideMain.unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import javax.annotation.PostConstruct;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.TourGuide.TourGuideMain.feignClient.GpsClient;
import com.TourGuide.TourGuideMain.feignClient.PricerClient;
import com.TourGuide.TourGuideMain.feignClient.RewardClient;
import com.TourGuide.TourGuideMain.helper.InternalTestHelper;
import com.TourGuide.TourGuideMain.manualMock.MockedGpsFeignClient;
import com.TourGuide.TourGuideMain.manualMock.MockedPricerFeignClient;
import com.TourGuide.TourGuideMain.manualMock.MockedRewardFeignClient;
import com.TourGuide.TourGuideMain.model.Attraction;
import com.TourGuide.TourGuideMain.model.Location;
import com.TourGuide.TourGuideMain.model.NearbyAttractions;
import com.TourGuide.TourGuideMain.model.Provider;
import com.TourGuide.TourGuideMain.model.User;
import com.TourGuide.TourGuideMain.model.VisitedLocation;
import com.TourGuide.TourGuideMain.service.TourGuideMainService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestTourGuideService {

	private static Logger logger = LoggerFactory.getLogger(TestTourGuideService.class);

    private static GpsClient gps;
	private static RewardClient reward;
	private static PricerClient pricer;

	private static TourGuideMainService service;

	User user;
	User user2;
	List<Attraction> attractions;
	VisitedLocation location;
	VisitedLocation location2;
	Map<UUID, Integer> rewardsMap;
	List<Provider> providersList;

	@PostConstruct
	private void setUp() {
		//Init Users
		user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

		//Init Attractions List
		attractions = new ArrayList<>();
		attractions.add(new Attraction("Dusneyland", "Paris", "France", 0.0D, 0.0D));
		attractions.add(new Attraction("Le Puy du Sain d'esprit", "Les Epesses", "France", 0.0D, 0.0D));
		attractions.add(new Attraction("Passoscope", "Chasseneuil-du-Poitou", "France", 0.0D, 0.0D));
		attractions.add(new Attraction("Park Asterux", "Paris", "France", 0.0D, 0.0D));
		attractions.add(new Attraction("Musée du Vrelou", "Paris", "France", 0.0D, 0.0D));
		attractions.add(new Attraction("Maisonnette de Chambord", "Chambord", "France", 0.0D, 0.0D));
		
		//Init VisitedLocation
		location = new VisitedLocation(user.getUserId(), new Location(0.0D, 0.0D), new Date());
		location2 = new VisitedLocation(user2.getUserId(), new Location(0.0D, 0.0D), new Date());

		//Init rewardsMap
		rewardsMap = new HashMap<>();
		rewardsMap.put(UUID.randomUUID(), 250);
		rewardsMap.put(UUID.randomUUID(), 500);

		//Init providersList
		providersList = new ArrayList<>();
		providersList.add(new Provider("FournisseurDeVancance Inc.", 299.99D, UUID.randomUUID().toString()));
		providersList.add(new Provider("VacancesDeFou", 249.99D, UUID.randomUUID().toString()));
		
		gps = new MockedGpsFeignClient(attractions);
		reward = new MockedRewardFeignClient(rewardsMap);
		pricer = new MockedPricerFeignClient(providersList);

		InternalTestHelper.setInternalUserNumber(0);
		service = new TourGuideMainService(gps, reward, pricer);
		
		service.addUser(user);
		service.addUser(user2);
	}

	@Test
	public void getUserLocation() throws InterruptedException, ExecutionException {
		VisitedLocation visitedLocation = service.trackUserLocation(user.getUserName()).get();
		assertTrue(visitedLocation.userId.equals(user.getUserId()));
	}
	
	@Test
	public void addUser() {
		service.addUser(user);
		service.addUser(user2);
		
		User retrivedUser = service.getUser(user.getUserName());
		User retrivedUser2 = service.getUser(user2.getUserName());
		
		assertEquals(user, retrivedUser);
		assertEquals(user2, retrivedUser2);
	}
	
	@Test
	public void getAllUsers() {
		service.addUser(user);
		service.addUser(user2);
		
		List<User> allUsers = service.getAllUsers();
		
		assertTrue(allUsers.contains(user));
		assertTrue(allUsers.contains(user2));
	}

	@Test
	public void getNearbyAttractions() throws InterruptedException, ExecutionException, Exception {
		NearbyAttractions attractions = service.getNearbyAttractions(user.getUserName()).get();
		assertEquals(5, attractions.getAttractionsList().size());
	}
	
	@Test
	public void getTripDeals() throws InterruptedException, ExecutionException {
		List<Provider> providers = service.getTripDeals(user.getUserName()).get();
		assertEquals(providersList, providers);
	}
	
}
