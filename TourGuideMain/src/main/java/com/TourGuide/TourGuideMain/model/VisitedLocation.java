package com.TourGuide.TourGuideMain.model;

import java.util.Date;
import java.util.UUID;

import com.TourGuide.TourGuideMain.annotation.ExcludeFromJacocoGeneratedReport;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString @Getter
@NoArgsConstructor @AllArgsConstructor
@ExcludeFromJacocoGeneratedReport
public class VisitedLocation {

  public UUID userId;
  
  public Location location;
  
  public Date timeVisited;

}