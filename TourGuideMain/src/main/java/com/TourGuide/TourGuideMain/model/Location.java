package com.TourGuide.TourGuideMain.model;

import com.TourGuide.TourGuideMain.annotation.ExcludeFromJacocoGeneratedReport;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@NoArgsConstructor @AllArgsConstructor
@ExcludeFromJacocoGeneratedReport
public class Location {
    public double longitude;
    
    public double latitude;
  }
