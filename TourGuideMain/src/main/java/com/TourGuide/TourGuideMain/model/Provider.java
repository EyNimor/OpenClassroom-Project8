package com.TourGuide.TourGuideMain.model;

import com.TourGuide.TourGuideMain.annotation.ExcludeFromJacocoGeneratedReport;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@NoArgsConstructor @AllArgsConstructor
@ExcludeFromJacocoGeneratedReport
public class Provider {
  public String name;
  
  public double price;
  
  public String tripId;
}

